package examenev03_hotelbaelish;

import static org.junit.jupiter.api.Assertions.*;

import org.cuatrovientos.hotel.baelish.Hotel;
import org.cuatrovientos.hotel.baelish.Room;
import org.junit.jupiter.api.Test;

class Hoteltest {
/***
 * Un test que instancie el hotel y busque un hu�sped. Obviamente no debe encontrarlo y la llamada deber�a devolver �NOT FOUND
 */
	@Test
	void testIniciarHotel() {
		Hotel target= new Hotel();
		String actual = target.searchGuest("degeg");
		String expected = "NOT FOUND";
		assertEquals(expected,actual);
		
	}
	/***
	 * Un test que instancie el hotel y a�ada un hu�sped. Al hacer una b�squeda debe devolver la habitaci�n en la que se ha a�adido ese hu�sped. 
	 */

	@Test
	void testBusquedaHuesped() {
		Hotel target = new Hotel();
		target.addGuest("Xabi", "Room10");
		String expected= "Room10";
		String actual = target.searchGuest("Xabi");
		assertEquals(expected,actual);	
	}
	/***
	 * Un test que instancie el hotel, a�ada un hu�sped. Luego debe eliminar ese hu�sped y al buscarlo la llamada deber�a devolver �NOT FOUND� 
	 */
	
	@Test
	void testEliminaci�nHuesped() {
		Hotel target = new Hotel();
		target.addGuest("Xabi", "Room10");
		target.delGuest("Xabi");
		String expected= "NOT FOUND";
		String actual = target.searchGuest("Xabi");
		assertEquals(expected,actual);
	}
	/***
	 * Un test que instancie el hotel y a�ada un hu�sped en una habitaci�n de tipo �Luxe�. Debes comprobar que el precio de esa habitaci�n es 90 
	 */
	@Test
	void testPreciohabitaci�n() {
		Hotel hotel = new Hotel();		
		Room room= new Room("Room58","Luxe");		
		int expected= 90;
		int actual= room.price();
		assertEquals(expected,actual);
	}

}
