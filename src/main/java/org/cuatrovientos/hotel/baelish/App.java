package org.cuatrovientos.hotel.baelish;

import java.util.Scanner;

/**
 * Main App for the hotel project
 * 
 * @author Eugenia Pérez
 */
public class App {
	private static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		int option = 0;
		Hotel hotel = new Hotel();

		do {
			showMenu();
			option = getOption();
			doAction(option, hotel);

		} while (option != 5);
	}

	/**
	 * @param reader
	 * @param option
	 * @param hotel
	 */
	private static void doAction(int option, Hotel hotel) {
		String name;
		String room;
		switch (option) {
		case 1:
			name = getName();
			room = getRoom();
			hotel.addGuest(name, room);
			break;
		case 2:
			name = getName();
			hotel.delGuest(name);
			break;
		case 3:
			name = getName();
			System.out.println(hotel.searchGuest(name));
			break;
		case 4:
			System.out.println(hotel.showAll());
			break;
		default:
			break;
		}
	}

	/**
	 * @param reader
	 * @return
	 */
	private static String getRoom() {
		System.out.println("Enter room:");
		String room = reader.nextLine();
		return room;
	}

	/**
	 * @param reader
	 * @return
	 */
	private static String getName() {
		System.out.println("Enter name:");
		return reader.nextLine();
	}

	/**
	 * @param reader
	 * @return
	 */
	private static int getOption() {
		String line = reader.nextLine();
		return Integer.parseInt(line);
	}

	/**
	 * 
	 */
	private static void showMenu() {
		System.out.println("1. Add guest");
		System.out.println("2. Delete guest");
		System.out.println("3. Search guest");
		System.out.println("4. Show guests");
		System.out.println("5. Exit");
	}
}
