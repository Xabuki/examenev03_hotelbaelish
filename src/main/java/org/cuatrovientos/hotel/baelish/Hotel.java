/**
 * 
 */
package org.cuatrovientos.hotel.baelish;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Represents a Hotel
 * 
 * @author Eugenia Pérez
 *
 */
public class Hotel {
	private Hashtable<String, Room> hotelRooms = new Hashtable<String, Room>();

	/**
	 * constructor
	 */
	public Hotel() {
		init();
	}

	/**
	 * inits the hotel with some rooms
	 */
	private void init() {
		hotelRooms.put("Room1", new Room("Room1", "Normal"));
		hotelRooms.put("Room2", new Room("Room2", "Normal"));
		hotelRooms.put("Room3", new Room("Room3", "Normal"));
		hotelRooms.put("Room4", new Room("Room4", "Normal"));
		hotelRooms.put("Room5", new Room("Room5", "Normal"));
		hotelRooms.put("Room6", new Room("Room6", "Normal"));
		hotelRooms.put("Room7", new Room("Room7", "Normal"));
		hotelRooms.put("Room8", new Room("Room8", "Normal"));
		hotelRooms.put("Room9", new Room("Room9", "Normal"));
		hotelRooms.put("Room10", new Room("Room10", "Normal"));
		hotelRooms.put("Room11", new Room("Room11", "Luxe"));
		hotelRooms.put("Room12", new Room("Room12", "Luxe"));
		hotelRooms.put("Room13", new Room("Room13", "Luxe"));
		hotelRooms.put("Room14", new Room("Room14", "Luxe"));
		hotelRooms.put("Room15", new Room("Room15", "Luxe"));
		hotelRooms.put("Room16", new Room("Room16", "Luxe"));
		hotelRooms.put("Room17", new Room("Room17", "Luxe"));
		hotelRooms.put("Room18", new Room("Room18", "Luxe"));
		hotelRooms.put("Room19", new Room("Room19", "Luxe"));
		hotelRooms.put("Room20", new Room("Room20", "Luxe"));
	}

	/**
	 * adds a guest to the hotel
	 * 
	 * @param guest
	 * @param room
	 */
	public void addGuest(String guest, String room) {
		hotelRooms.get(room).setGuest(guest);
	}

	/**
	 * deletes a guest from the hotel
	 * 
	 * @param guest
	 */
	public void delGuest(String guest) {
		Enumeration<String> keys = hotelRooms.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (hotelRooms.get(key).getGuest().equals(guest)) {
				hotelRooms.get(key).setGuest("");
				break;
			}
		}

		// Otra forma
		// Enumeration<Room> elements = hotelRooms.elements();
		// Room room = null;
		// while (elements.hasMoreElements()) {
		// room = elements.nextElement();
		// if (room.getGuest().equals(guest)) {
		// room.setGuest("");
		// break;
		// }
		// }
	}

	/**
	 * performs a search in the hotel
	 * 
	 * @param guest
	 * @return the room where the guest is or "NOT FOUND"
	 */
	public String searchGuest(String guest) {
		Enumeration<String> keys = hotelRooms.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			if (hotelRooms.get(key).getGuest().equals(guest)) {
				return key;
			}
		}
		return "NOT FOUND";

		// Otra forma
		// Enumeration<Room> elements = hotelRooms.elements();
		// Room room = null;
		// while (elements.hasMoreElements()) {
		// room = elements.nextElement();
		// if (room.getGuest().equals(guest)) {
		// return room.getRoomName();
		// }
		// }
		// return "NOT FOUND";
	}

	/**
	 * show all information from hotels
	 * 
	 * @return all information
	 */
	public String showAll() {
		String result = "";
		Enumeration<String> keys = hotelRooms.keys();
		while (keys.hasMoreElements()) {
			String key = keys.nextElement();
			result += hotelRooms.get(key).toString() + "\n";
		}
		return result;

		// Otra forma
		// Enumeration<Room> elements = hotelRooms.elements();
		// Room room = null;
		// while (elements.hasMoreElements()) {
		// room = elements.nextElement();
		// result += room.toString() + "\n";
		// }
		//
		// return result;
	}

	/**
	 * @return the hotelRooms
	 */
	public Hashtable<String, Room> getHotelRooms() {
		return hotelRooms;
	}

	/**
	 * @param hotelRooms
	 *            the hotelRooms to set
	 */
	public void setHotelRooms(Hashtable<String, Room> hotelRooms) {
		this.hotelRooms = hotelRooms;
	}

}
