/**
 * 
 */
package org.cuatrovientos.hotel.baelish;

/**
 * Represents a Room
 * @author Eugenia Pérez
 *
 */
public class Room {
	private static final int LUXE_ROOM_PRICE = 90;
	private static final int NORMAL_ROOM_PRICE = 20;
	private String roomName;
	private String guest;
	private String roomType;

	
	/**
	 * Constructor
	 * @param roomName
	 * @param roomtype
	 */
	public Room(String roomName, String roomType) {
		this.roomName = roomName;
		this.roomType = roomType;
		this.guest = "";
	}
	
	/**
	 * gives the price of the room according to its type
	 * @return
	 */
	public int price () {
		if (roomType.equals("Normal")) {
			return NORMAL_ROOM_PRICE;
		} else {
			return LUXE_ROOM_PRICE;
		}
	}

	/****** GETTERS/SETTERS ********/

	public String getRoomName() {
		return roomName;
	}


	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}


	public String getGuest() {
		return guest;
	}


	public void setGuest(String guest) {
		this.guest = guest;
	}


	public String getRoomtype() {
		return roomType;
	}


	public void setRoomType(String roomType) {
		this.roomType = roomType;
	}


	@Override
	public String toString() {
		return "Room [roomName=" + roomName + ", guest=" + guest
				+ ", roomType=" + roomType + "]";
	}
	
	

}
